class ContactUs < ApplicationRecord
  validates :name, :mobile_number, :message, presence: true
  validates :mobile_number, uniqueness: true, numericality: true, length: { minimum: 10, maximum: 10 }
end
