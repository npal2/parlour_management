class ContactUsController < ApplicationController
  def index
  end

  def create
    contact_us = ContactUs.new(contact_us_params)
    if contact_us.save
      flash[:success] = 'Details save successfully!'
    else
      flash[:error] = contact_us.errors.full_messages
    end
    redirect_to contact_us_path
  end

  private

  def contact_us_params
    params.permit(:name, :mobile_number, :message)
  end
end
