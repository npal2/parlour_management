class CreateContactUs < ActiveRecord::Migration[6.1]
  def change
    create_table :contact_us do |t|
      t.string :name, null: false, default: ''
      t.string :mobile_number, null: false, default: ''
      t.text :message, null: false, default: ''
      t.timestamps
    end
  end
end
